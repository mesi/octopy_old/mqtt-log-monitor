import json
import argparse
import logging
from os import path
from datetime import datetime
from lib.octopyapp.octopyapp import OctopyApp

APP_ID = 'log monitor'
LOG_LEVEL = logging.DEBUG




class LogMonitor(OctopyApp):
    def __init__(self, config):
        super().__init__(APP_ID, LOG_LEVEL, config)
        self.nodes = {}
        topic = path.join(self.topics['logging']['prefix'], '#')
        self.subscribe(topic)


    def on_message(self, client, userdata, msg):
        try:
            payload = json.loads(msg.payload)
            level = msg.topic.split('/')[-1].upper()
            timestamp = datetime.now().isoformat()
            print(f"{timestamp} [{payload['source'].upper()}] {level}: {payload['message']}")
        except BaseException as e:
            print(f"Error receiveing log message: {e}")



if __name__ == '__main__':
    arg_parser = argparse.ArgumentParser()
    arg_parser.add_argument('--host',               help="MQTT host/IP", type=str, default="localhost")
    arg_parser.add_argument('--port',               help="MQTT port", type=int, default=1883)
    arg_parser.add_argument('--username', '--user', help="MQTT username", type=str, default="")
    arg_parser.add_argument('--password', '--pass', help="MQTT password", type=str, default="")
    arg_parser.add_argument('--prefix',             help="MQTT prefix", type=str, default="")
    args = arg_parser.parse_args()
    config = {
        'mqtt': {}
    }
    if args.host:
        config['mqtt']['host'] = args.host
    if args.port:
        config['mqtt']['port'] = args.port
    if args.username:
        config['mqtt']['username'] = args.username
    if args.password:
        config['mqtt']['password'] = args.password
    if args.prefix:
        config['mqtt']['prefix'] = args.prefix

    lm = LogMonitor(config)
    lm.start()
